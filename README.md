# Games REST Endpoint

### Technology

Java Spring Boot Framework with H2 RDBMS and Gradle. Added Python script to collect seed game data from [https://www.igdb.com/](https://www.igdb.com/).

### Running the Endpoint

In project directory:

```
 $ ./gradlew bootRun
```

Or to build executable jar file

```
$ ./gradlew bootJar
$ java -jar build/libs/CHGames-0.0.1-SNAPSHOT.jar
```

#### Data

Database can be seen directly at:

[http://localhost:8080/h2](http://localhost:8080/h2)

with

```
JDBC URL: jdbc:h2:./data/chgames
User Name: sa
Password: passw0rd13
```

#### Task 1

There are 52 games available:

[http://localhost:8080/games/1](http://localhost:8080/games/1)


#### Task 2

All games have a value, including those without any comments:

[http://localhost:8080/games/report](http://localhost:8080/games/report)

