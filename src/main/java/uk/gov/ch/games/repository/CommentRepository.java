package uk.gov.ch.games.repository;

import org.springframework.data.repository.CrudRepository;

import uk.gov.ch.games.model.Comment;

/**
* Comment Entity repository
* 
*/
public interface CommentRepository extends CrudRepository<Comment, Long> {

	Comment findById(long id);
	
}

