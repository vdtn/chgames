package uk.gov.ch.games.model;

/**
* Platform enumeration used in {@link Game}
* 
*/
public enum Platform {
	PS3,//0
	PS4,//1
	Xbox360,//2
	XboxOne,//3
	PC,//4
	Mac,//5
	Wii,//6
	NES,//7
	Switch,//8
	iOS,//9
	Android//10
}
