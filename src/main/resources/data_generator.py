import requests
import json
import random
from faker import Faker

fake = Faker()

# utility function
def age_rating_switch(i):
    switcher={1:3,2:7,3:12,4:16,5:18}
        return switcher.get(i,"Invalid age rating")

#reviewers names
reviewers = ['bob','testingPriest'] + [fake.simple_profile()['username'] for i in range(10)]

# fetching game data, only the ones with companies and summaries, etc
headers = {'user-key': '71162e4f3ae4d871e0f18a3f8699a8c2'} # this is my private key, please be nice
response = requests.get('https://api-v3.igdb.com/games', headers=headers, data="fields id, name, age_ratings.rating, summary, involved_companies.company.name, platforms.name, popularity, rating;where involved_companies !=n;where platforms !=n & summary !=n & involved_companies  !=n & rating > 50 & age_ratings.rating = (1,2,3,4,5); limit 50;")

if response.status_code == 200:
    
    data = json.loads(response.text)

    reviewid = 3
        
        for i, r in enumerate(data):

gameid = i+3
    
    for a in r["age_ratings"]:
        if a["rating"] < 6:
            age_rating = age_rating_switch(a["rating"])

#quickly sanitsed summary
summary = repr(r["summary"].split('\n', 1)[0][:100].replace('"','').replace('\'',''))
title = r["name"].replace('\'','')

gamesql = f'INSERT INTO game (id, title, description, by,age_rating,likes) VALUES ({gameid}, \'{title}\', {summary}, \'{r["involved_companies"][0]["company"]["name"]}\',\'{age_rating}\',{int(r["rating"])});'
    
    print(gamesql)
    
    for platformid in random.sample(range(9),random.randrange(1,4)):
        platformsql = f'INSERT INTO game_platforms (game_id,platforms) VALUES ({gameid},{platformid});'
            print(platformsql)

#sample of 1 to 9 reviewers
for reviewer in random.sample(reviewers, random.randrange(1,9)):
    
    timestamp = int(fake.date_time_between(start_date="-5y", end_date="now").timestamp());
    
    reviewsql = f'INSERT INTO comment (id, user, message, utildate, likes,game_id) VALUES ({reviewid},\'{reviewer}\',\'{fake.sentence()}\',{timestamp},{random.randrange(0,10)},{gameid});'
        
        print(reviewsql)
        
            reviewid +=1
