package uk.gov.ch.games.model;

import java.util.Collection;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;


/**
* Game is the main entity
* 
* It references {@link Comment} and {@link Platform}
* 
* Expected json representation:
* 
* 		{
		"title": "Uncharted 4",
	        "description": "For the first time ever in Uncharted history, drive vehicles during gameplay",
	        "by": "Sony",
	        "platform": ["PS4"],
	        "age_rating": "16",
	        "likes": 100,
	        "comments": [{
	            "user": "bob",
	            "message": "Cracking game far too much cinematic",
	            "dateCreated": "2011-01-03",
	            "like": 6
	        }, {
	            "user": "testingPriest",
	            "message": "Not enough shooting for me,far too easy ",
	            "dateCreated": "2011-04-02",
	            "like": 5
	        }]
	    }
* 
*/
@Data
@Entity
@JsonPropertyOrder({ "title", "description", "by", "platform", "age_rating", "likes", "comments" })
public class Game {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonIgnore
    private Long id;
	
    private String title;
	
    private String description;
    private String by;

    @ElementCollection(fetch = FetchType.EAGER)
    @JsonProperty("platform")
    Collection<Platform> platforms;
    
    private String age_rating;
    private int likes;
    
    @OneToMany(mappedBy = "game")
    Collection<Comment> comments;
    
}
