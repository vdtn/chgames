package uk.gov.ch.games;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.jayway.jsonpath.JsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class ChGamesApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	void contextLoads() {
	}
	
	@Test
	public void should_get_game_and_return_ok_status() throws Exception {

		MvcResult rez = mockMvc
				.perform(get("/games/1"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.title").exists())
				.andExpect(jsonPath("$.description").exists())
				.andExpect(jsonPath("$.by").exists())
				.andExpect(jsonPath("$.platform").isArray())
				.andExpect(jsonPath("$.platform").isNotEmpty())
				.andExpect(jsonPath("$.age_rating").exists())
				.andExpect(jsonPath("$.likes").exists())
				.andExpect(jsonPath("$.comments").isArray())
				.andExpect(jsonPath("$.comments").isNotEmpty())
				.andExpect(jsonPath("$.comments[0].user").exists())
				.andExpect(jsonPath("$.comments[0].message").exists())
				.andExpect(jsonPath("$.comments[0].dateCreated").exists())
				.andExpect(jsonPath("$.comments[0].like").exists())
				.andReturn();

		String title = JsonPath.read(rez.getResponse().getContentAsString(), "$.title");
		
		int bobLikes = JsonPath.read(rez.getResponse().getContentAsString(), "$.comments[0].like");
		
		String bobDate = JsonPath.read(rez.getResponse().getContentAsString(), "$.comments[0].dateCreated");

		Assumptions.assumeTrue(title.equals("Uncharted 4"));
		Assumptions.assumeTrue(bobLikes == 6);
		Assumptions.assumeTrue(bobDate.equals("2011-01-03"));
	}
	
	@Test
	public void should_get_report_and_return_ok_status() throws Exception {

		MvcResult rez = mockMvc
				.perform(get("/games/report"))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.user_with_most_comments").exists())
				.andExpect(jsonPath("$.highest_rated_game").exists())
				.andExpect(jsonPath("$.average_likes_per_game").isArray())
				.andExpect(jsonPath("$.average_likes_per_game").isNotEmpty())
				.andExpect(jsonPath("$.average_likes_per_game[0].title").exists())
				.andExpect(jsonPath("$.average_likes_per_game[0].average_likes").exists())
				.andReturn();

	}

}
