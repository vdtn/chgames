package uk.gov.ch.games.model;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import lombok.Data;
import uk.gov.ch.games.model.MyLocalDateTimeSerializer;

/**
* Comment represents game comments
* 
* It is referenced by {@link Game}
* 
* Expected json representation:
* 
* 	{
        "user": "bob",
        "message": "Cracking game far too much cinematic",
        "dateCreated": "2011-01-03",
        "like": 6
    }
* 
*/
@Data
@Entity
public class Comment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@JsonIgnore
	private Long id; 
	
	private String user;
	
	private String message;
	
	@Column(name="utildate")
	@JsonProperty("dateCreated")
	@JsonSerialize(using = MyLocalDateTimeSerializer.class)
    private long utilDate;
	
	@ManyToOne
	@JsonIgnore
    private Game game;
	
	@JsonProperty("like")
	private int likes;


}
/**
 * 
 * Custom serializer for comment date epoch to yyyy-MM-dd
 *
 */
class MyLocalDateTimeSerializer extends JsonSerializer<Long> {

	@Override
	public void serialize(Long value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		LocalDateTime dt = LocalDateTime.ofInstant(Instant.ofEpochSecond(value), ZoneId.systemDefault());
		gen.writeString(dt.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
	}
}

