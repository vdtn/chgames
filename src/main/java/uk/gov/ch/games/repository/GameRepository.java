package uk.gov.ch.games.repository;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.fasterxml.jackson.annotation.JsonProperty;

import uk.gov.ch.games.model.Game;

/**
* Game Entity repository
* 
*/
public interface GameRepository extends CrudRepository<Game, Long> {

	Game findById(long id);
	
	@Query(value = "SELECT TOP 1 user FROM COMMENT  GROUP BY user ORDER BY count(user) DESC", nativeQuery = true)
	String getUserWithMostComments();

	@Query(value = "SELECT TOP 1 g.title FROM COMMENT c, GAME g  WHERE c.game_id = g.id GROUP BY game_id ORDER BY sum(c.likes) DESC", nativeQuery = true)
	String getHighestRatedGame();
	
	
	public interface AverageLikePerGame {
		String getTitle();
		@JsonProperty("average_likes")
		int getAveragelikes();
	}
	
	//This query didn't handle the case where there are no comments
	//@Query(value = "SELECT g.title as title, avg(c.likes) as averagelikes FROM COMMENT c, GAME g  WHERE c.game_id = g.id GROUP BY c.game_id ORDER BY title", nativeQuery = true)
	@Query(value = "SELECT g.title as title, (CASE WHEN avg(c.likes) IS NULL THEN 0 ELSE avg(c.likes) END)  as averagelikes FROM GAME g LEFT JOIN COMMENT c ON g.id = c.game_id GROUP BY c.game_id ORDER BY title", nativeQuery = true)
	public List<AverageLikePerGame> getAverageLikePerGame();
}

