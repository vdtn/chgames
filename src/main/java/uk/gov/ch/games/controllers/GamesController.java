package uk.gov.ch.games.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import uk.gov.ch.games.model.Game;
import uk.gov.ch.games.repository.GameRepository;

@RestController
@RequestMapping("/games")
public class GamesController {
	
	@Autowired private GameRepository gameRepository;


	@RequestMapping(value="/{gameId}", method = RequestMethod.GET)
	public ResponseEntity<Object> getGame(@PathVariable("gameId") Long gameId) {

		Optional<Game> game = gameRepository.findById(gameId);
		if (game.isPresent()) {
			return new ResponseEntity<>(game.get(),HttpStatus.OK);
		}
		return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
	}
	
	@RequestMapping(value="/report", method = RequestMethod.GET)
	public ResponseEntity<Report> getReport() {
		
		Report report = new Report();
		
		report.setUserWithMostComments(gameRepository.getUserWithMostComments());
		report.setHighestRatedGame(gameRepository.getHighestRatedGame());
		report.setAverageLikesPerGame(gameRepository.getAverageLikePerGame());

		
		return new ResponseEntity<Report>(report,HttpStatus.OK);
		
	}
	
	/**
	 * 
	 *Result class for /games/report
	 *
	 *Json should match:
	 *{
		    "user_with_most_comments": "Alison",
		    "highest_rated_game": "Uncharted 4",
		    "average_likes_per_game": [
		        {
			    "title": "Call of Duty, Infinite Warfare",
			    "average_likes": 4,
			},
			{
			    "title": "Uncharted 4",
			    "average_likes": 7,
			}
		    ] 
		}
	 *
	 */
	@Data
	public class Report {
		
		@JsonProperty("user_with_most_comments")
		String userWithMostComments;
		
		@JsonProperty("highest_rated_game")
		String highestRatedGame;
		
		@JsonProperty("average_likes_per_game")
		List<GameRepository.AverageLikePerGame> averageLikesPerGame;
		
	}

	
}